"use strict";

var data = [{
  name: 'Regina',
  base: 'tomate',
  price_small: 6.5,
  price_large: 9.95,
  image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
}, {
  name: 'Napolitaine',
  base: 'tomate',
  price_small: 6.5,
  price_large: 8.95,
  image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
}, {
  name: 'Spicy',
  base: 'crème',
  price_small: 5.5,
  price_large: 8,
  image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300'
}];
var character = {
  firstName: 'Skyler',
  lastName: 'White'
};
var firstName = character.firstName,
    lastName = character.lastName;
var html = '';

function compareName(a, b) {
  if (a.name < b.name) {
    return -1;
  } else if (a.name > b.name) {
    return 1;
  }
}

function comparePriceLarge(a, b) {
  if (a.price_large < b.price_large) {
    return -1;
  } else if (a.price_large > b.price_large) {
    return 1;
  }
}

function comparePriceSmall(a, b) {
  if (a.price_small < b.price_small) {
    return -1;
  } else if (a.price_small > b.price_small) {
    return 1;
  }
}

var result = data.filter(containsDoubleI);

function baseTomate(base) {
  if (base.base == "tomate") {
    return true;
  } else {
    return false;
  }
}

function containsDoubleI(base) {
  var res = base.name;
  var cpt = 0;

  for (var i = 0; i < res.length; i++) {
    if (res.charAt(i) == 'i') {
      cpt++;
    }
  }

  if (cpt == 2) {
    return true;
  } else {
    return false;
  }
}

function petitprix(base) {
  if (base.price_small <= 6) {
    return true;
  } else {
    return false;
  }
} // data.sort(compareName);


result.forEach(function (pizza) {
  html += "<article class=\"pizzaThumbnail\">\n<a href=\"".concat(pizza.image, "\">\n\t\t<img src=\"").concat(pizza.image, "\" />\n\t\t<section>\n\t\t\t<h4>").concat(pizza.name, "</h4>\n\t\t\t<ul>\n\t\t\t\t<li>Prix petit format : ").concat(pizza.price_small, " \u20AC</li>\n\t\t\t\t<li>Prix grand format : ").concat(pizza.price_large, " \u20AC</li>\n\t\t\t</ul>\n\t\t</section>\n\t</a>\n\n</article>");
});
document.querySelector('.pageContent').innerHTML = html;
//# sourceMappingURL=main.js.map