const data = [
	{
		name: 'Regina',
		base: 'tomate',
		price_small: 6.5,
		price_large: 9.95,
		image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	},
	{
		name: 'Napolitaine',
		base: 'tomate',
		price_small: 6.5,
		price_large: 8.95,
		image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
	},
	{
		name: 'Spicy',
		base: 'crème',
		price_small: 5.5,
		price_large: 8,
		image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
	}
];

// const character = { firstName: 'Skyler', lastName: 'White' };
// const firstName = character.firstName,
	// lastName = character.lastName;
// const { firstName, lastName } = character;

function kill(character) {
        console.log( `${character.firstName} ${character.lastName} is dead :'(` );
    }

function kill({firstName, lastName}) {
	console.log( `${firstName} ${lastName} is dead :'(` );
}


let html='';

function compareName({name}, pizza) {
    if(name<pizza.name){
        return -1;
    }else if (name>pizza.name){
        return 1;
    }else{
        return 0;
    }
}
function comparePriceLarge({price_large}, b) {
    if(price_large<b.price_large){
        return -1;
    }else if (price_large>b.price_large){
        return 1;
    }else{
        return 0;
    }
}

function comparePriceSmall({price_small},b) {
    if(price_small<b.price_small){
        return -1;
    }else if (price_small>b.price_small){
        return 1;
    }else{
        return 0;
    }
}
let result =data.filter(petitprix);

function baseTomate(base) {
    if(base.base == "tomate"){
        return true;
    }else{
        return false;
    }
}
function containsDoubleI({name}) {
   const res= name.split('i').length-1;

   if(res ==2){
       return true;
   }else{
       return false;
   }
    
}
function petitprix({price_small}) {
    if(price_small<=6){
        return true;
    }else{
        return false;
    }
}


// data.sort(compareName);

data.forEach(({name,price_small,price_large,image}) =>{
html += `<article class="pizzaThumbnail">
<a href="${image}">
		<img src="${image}" />
		<section>
			<h4>${name}</h4>
			<ul>
				<li>Prix petit format : ${price_small} €</li>
				<li>Prix grand format : ${price_large} €</li>
			</ul>
		</section>
	</a>

</article>`;   
});

document.querySelector('.pageContent').innerHTML = html;
